<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
Route::get('search/{term}', 'ApiController@find');
Route::get('business/{id}', 'ApiController@getOne'); 
 
Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'ApiController@logout');
 
    Route::get('user', 'ApiController@getAuthUser');
    // Route::get('business/{id}', 'BusinessController@getOne'); 
    Route::get('businesses', 'BusinessController@index');
    Route::post('add', 'BusinessController@store');
    Route::put('update/{id}', 'BusinessController@update');
    Route::delete('remove/{id}', 'BusinessController@remove');
});
