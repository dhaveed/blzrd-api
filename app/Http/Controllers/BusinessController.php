<?php
namespace App\Http\Controllers;
use App\business;
use Illuminate\Http\Request;
use JWTAuth;

class BusinessController extends Controller
{
    protected $user;
 
    public function __construct(){
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        return $this->user
            ->businesses()
            ->get(['name', 'location', 'employees', 'email', 'phone', 'industry', 'about'])
            ->toArray();
    }

    public function getOne($id)
    {
        $business = $this->user->businesses()->find($id);
    
        if (!$business) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, business with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return $business;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'industry' => 'required|string',
            'location' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|string',
            'about' => 'required|string',
            'employees' => 'required|string'
        ]);
    
        $business = new Business();
        $business->name = $request->name;
        $business->industry = $request->industry;
        $business->location = $request->location;
        $business->phone = $request->phone;
        $business->email = $request->email;
        $business->about = $request->about;
        $business->employees = $request->employees;
    
        if ($this->user->businesses()->save($business))
            return response()->json([
                'success' => true,
                'business' => $business
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, business could not be saved'
            ], 500);
    }

    public function update(Request $request, $id)
    {
        $business = $this->user->businesses()->find($id);
    
        if (!$business) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, business with id ' . $id . ' was not found'
            ], 400);
        }
    
        $updated = $business->fill($request->all())
            ->save();
    
        if ($updated) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, business could not be updated'
            ], 500);
        }
    }

    public function remove($id)
    {
        $business = $this->user->businesses()->find($id);
    
        if (!$business) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, business with id ' . $id . ' was not found'
            ], 400);
        }
    
        if ($business->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Business could not be deleted'
            ], 500);
        }
    }

    // public function find($term)
    // {
    //     $business = $this->user
    //                 ->businesses()
    //                 ->where('name', 'like', "%{$term}%")
    //                 ->orWhere('location', 'like', "%$term%")
    //                 ->orWhere('industry', 'like', "%$term%")
    //                 ->get();
    
    //     if (!$business) {
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Sorry, no result was found for ' . $term
    //         ], 400);
    //     }
    
    //     return $business;
    // }
    
}
